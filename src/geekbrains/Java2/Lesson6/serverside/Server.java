package geekbrains.Java2.Lesson6.serverside;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class Server {
    private ServerSocket serverSocket;
    private Socket socket;
    private PrintWriter out;
    private Scanner in;

    public Server() {
        try {
            this.serverSocket = new ServerSocket(8599);
            System.out.println("Server started! Waiting clients ...");
            this.socket = serverSocket.accept(); // ожидание подключений, приложение ставится на паузу. Как при system.in
            System.out.println("Client connected!");
            in = new Scanner(socket.getInputStream()); // обработчик входящей информации по сокету
            out = new PrintWriter(socket.getOutputStream()); // обработчик исходящей информации по сокету

            new Thread(new Runnable() {
                @Override
                public void run(){
                    while (true) {
                        String string = in.nextLine();
                        System.out.println(string);
//                printWriter.println("Server " + string); //складывается в буфер
//                printWriter.flush(); //очистка буфера и отправка всего что в нем хранится
                    }
                }
            }).start();

            while (true) {
                String string = new Scanner(System.in).nextLine();
                out.println(string);
                out.flush();
            }

        } catch (IOException e) {
            System.out.println("Initializatipon error");
        } finally {
            try {
                serverSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}