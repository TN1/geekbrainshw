package geekbrains.Java2.Lesson6.clientside;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    private final String SERVER_ADDRESS = "localhost";
    private final int SERVER_PORT = 8599;
    private Socket socket;
    private Scanner in;
    private PrintWriter out;

    public Client() {
        try {
            socket = new Socket(SERVER_ADDRESS, SERVER_PORT);
            in = new Scanner(socket.getInputStream());
            out = new PrintWriter(socket.getOutputStream());
            Scanner scanner = new Scanner(System.in);

            new Thread(new Runnable() {
                @Override
                public void run(){
                    while (true) {
                        String string = in.nextLine();
                        System.out.println(string);
                    }
                }
            }).start();

            while (true) {
                String string = scanner.nextLine();
                out.println(string);
                out.flush();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
