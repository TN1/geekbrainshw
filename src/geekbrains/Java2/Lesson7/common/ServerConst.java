package geekbrains.Java2.Lesson7.common;

public interface ServerConst {
    int PORT = 8899;
    String SERVER_URL = "localhost";
}