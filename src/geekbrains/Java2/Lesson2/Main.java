package geekbrains.Java2.Lesson2;

public class Main {
    public static void main(String[] args) {
        String[][] array1 = {{"123", "23", "34", "6"}, {"we", "4", "a", "vasya"}, {"1", "2", "3", "4"}, {"10", "5", "60", "65"}};
        String[][] array2 = {{"1", "2", "3", "4"}, {"5", "4", "6", "7"}, {"1", "2", "3", "4"}, {"10", "5", "60", "65"}};
        String[][] array3 = {{"5", "5", "44"}, {"1", "4", "5", "10"}, {"1", "2", "3"}, {"10", "5", "60", "65"}};

        try {
            summArrayElements(array1);
        } catch (MyArraySizeException e) {
            e.printStackTrace();
        }

        try {
            summArrayElements(array2);
        } catch (MyArraySizeException e) {
            e.printStackTrace();
        }

        try {
            summArrayElements(array3);
        } catch (MyArraySizeException e) {
            e.printStackTrace();
        }
    }

    public static void summArrayElements(String[][] array) throws MyArraySizeException{
        System.out.println("**************************");
        if (array.length != 4 || array[0].length != 4) {
            throw new MyArraySizeException();
        }
        int summElements = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                try {
                    summElements += stringToInt(array[i][j], i, j);
                } catch (MyArrayDataException e) {
                    e.printStackTrace();
                }
            }
        }
        System.out.println("Summ of elements = " + summElements);
    }

    public static int stringToInt(String str, int i, int j) throws MyArrayDataException{
        try {
            return Integer.parseInt(str);
        } catch (NumberFormatException e) {
            throw new MyArrayDataException(i, j, str);
        }
    }
}