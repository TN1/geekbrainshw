package geekbrains.Java2.Lesson2;

public class MyArrayDataException extends RuntimeException {
    private int row;
    private int column;
    private String element;

    public MyArrayDataException(int row, int column, String element) {
        this.row = row;
        this.column = column;
        this.element = element;
    }

    @Override
    public  String toString() {
        return "Incorrect element in [" + row + "][" + column + "]: " + element;
    }
}
