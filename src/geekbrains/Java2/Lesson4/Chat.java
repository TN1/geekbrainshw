package geekbrains.Java2.Lesson4;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Chat extends JFrame {
    public Chat() {
        setTitle("GeekBrains Chat v.0.1");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(800, 600);
        setLocationRelativeTo(null);        //window in center
        setResizable(true);     //change size
        setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));

        JTextField textField = new JTextField();
        textField.setPreferredSize(new Dimension(680, 30));
        JButton button = new JButton();
        button.setPreferredSize(new Dimension(80, 28));
        button.setText("Send");

        JPanel panelSend = new JPanel();
        panelSend.add(textField, BorderLayout.WEST);
        panelSend.add(button, BorderLayout.EAST);

        JTextArea textArea = new JTextArea();
        textArea.setPreferredSize(new Dimension(765, 505));
        textArea.setEditable(false);
        textArea.setLineWrap(true);
        JScrollPane scrollPanel = new JScrollPane(textArea);
        scrollPanel.setPreferredSize(new Dimension(765, 505));

        JPanel panelText = new JPanel();
        panelText.add(scrollPanel, BorderLayout.CENTER);

        textField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e){
                textArea.append(textField.getText() + "\n");
                textArea.setCaretPosition(textArea.getDocument().getLength());
                textField.setText("");
            }
        });

        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                textArea.append(textField.getText() + "\n");
                textArea.setCaretPosition(textArea.getDocument().getLength());
                textField.setText("");
            }
        });

        add(panelText);
        add(panelSend);

        setVisible(true);
    }
}