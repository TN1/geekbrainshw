package geekbrains.Java2.Lesson5;

/**
 * Created by Eliseev Vasiliy on 04.02.2018.
 */
public class Main {
    public static void main(String[] args) {
        final int size = 10_000_000;
        float[] arr = new float[size];
        method1(arr);
        testResult(arr);
        System.out.println("\n**************************************************");
        method2(arr);
        testResult(arr);
    }

    private static void method1(float[] arr) {
        for (int i = 0; i < arr.length; i++) {
            arr[i] = 1;
        }
        long a = System.currentTimeMillis();
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (float)(arr[i] * Math.sin(0.2f + i / 5) * Math.cos(0.2f + i / 5) * Math.cos(0.4f + i / 2));
        }
        System.out.println("Method 1:");
        System.out.println("Completed in " + (System.currentTimeMillis() - a)/1000.0 + " seconds");
    }

    private static void method2(float[] arr) {
        int h = arr.length / 2;
        for (int i = 0; i < arr.length; i++) {
            arr[i] = 1;
        }

        long a = System.currentTimeMillis();
        float[] halfArr1 = new float[h];
        float[] halfArr2 = new float[h];
        System.arraycopy(arr, 0, halfArr1, 0, h);
        System.arraycopy(arr, h, halfArr2, 0, h);
        
        Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < halfArr1.length; i++) {
                    halfArr1[i] = (float)(halfArr1[i] * Math.sin(0.2f + i / 5) * Math.cos(0.2f + i / 5) * Math.cos(0.4f + i / 2));
                }
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        Thread thread2 = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < halfArr2.length; i++) {
                    halfArr2[i] = (float)(halfArr2[i] * Math.sin(0.2f + (i + h) / 5) * Math.cos(0.2f + (i + h) / 5) * Math.cos(0.4f + (i + h) / 2));
                }
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        try {
            thread1.start();
            thread2.start();
            thread1.join();
            thread2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.arraycopy(halfArr1, 0, arr, 0, h);
        System.arraycopy(halfArr2, 0, arr, h, h);

        System.out.println("Method 2:");
        System.out.println("Completed in " + (System.currentTimeMillis() - a)/1000.0 + " seconds");
    }

    public static void testResult(float[] arr) {
        for (int i = 0; i < arr.length; i += 471_255) {
            System.out.print(arr[i] + " ");
        }
    }
}