package geekbrains.Java2.Lesson3;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Phonebook {

    private HashMap<String, ArrayList<String>> book = new HashMap<>();

    public void add(String surname, String phone) {
        if (!book.containsKey(surname)) {
            ArrayList<String> value = new ArrayList<>();
            value.add(phone);
            book.put(surname, value);
        } else {
            book.get(surname).add(phone);
        }
    }

    public void get(String surname) {
        System.out.println();
        System.out.println("Search for " + surname + ":");
        for (int i = 0; i < book.get(surname).size(); i++) {
            System.out.println(book.get(surname).get(i));
        }
    }

    public void printBook(){
        for (Map.Entry<String, ArrayList<String>> iter : book.entrySet()) {
            System.out.println(iter.getKey() + " - " + iter.getValue());
        }
    }
}