package geekbrains.Java2.Lesson3;

import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        String[] array = {"dron", "table", "dress", "egg", "film", "brain", "steel", "universe", "bill", "table", "brain",
                "video", "tron", "brain", "film", "soft", "hard", "lift", "catalog", "log", "brain", "table", "steel"};

        countWord(array);

        Phonebook phonebook = new Phonebook();
        phonebook.add("Eliseev", "+79652112274");
        phonebook.add("Eliseev", "+79165671287");
        phonebook.add("Eliseev", "+79858906543");
        phonebook.add("Lermontov", "+487612335634");
        phonebook.add("London", "+85634442389");
        phonebook.add("Tolstoy", "+878537653343");
        phonebook.add("Tolstoy", "+8785657333");
        phonebook.add("Fet", "+847568735438");
        phonebook.add("Tyrgenev", "+83654763454");
        phonebook.add("Bylgakov", "+37654376542");
        phonebook.add("Strygackie", "+7654764537");
        phonebook.add("Strygackie", "+863726547624");
        phonebook.add("Strygackie", "+73654763443");
        phonebook.add("Palanic", "+234515653334");

        System.out.println("********************************************");
        phonebook.printBook();
        phonebook.get("Eliseev");
        phonebook.get("Fet");
        phonebook.get("Tolstoy");
    }

    private static void countWord(String[] array) {
        HashMap<String, Integer> wordsMap = new HashMap<>();
        int count = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                if (array[i].equals(array[j])) {
                    count++;
                }
            }
            wordsMap.put(array[i], count);
            count = 0;
        }

        System.out.println("Count of words:");
        for (Map.Entry<String, Integer> iter : wordsMap.entrySet()) {
            System.out.println(iter.getKey() + " - " + iter.getValue());
        }
    }
}
