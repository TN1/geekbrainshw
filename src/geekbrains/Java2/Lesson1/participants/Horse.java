package geekbrains.Java2.Lesson1.participants;

public class Horse extends Animal {
    public Horse(int maxRunDistance, int maxJumpHeight, int maxSwimDistance, String name) {
        super(maxRunDistance, maxJumpHeight, maxSwimDistance, name);
    }

    @Override
    public void printAboutInfo() {
        System.out.println("Horse "+ name);
    }
}