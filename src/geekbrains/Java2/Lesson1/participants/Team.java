package geekbrains.Java2.Lesson1.participants;

public class Team  {
    private String teamName;
    private iChallengable[] players = new iChallengable[4];
    private boolean onDistance;

    public Team(String teamName, iChallengable[] players) {
        this.teamName = teamName;
        this.players = players;
        this.onDistance = true;
    }

    public iChallengable[] getPlayers() {
        return players;
    }

    public void printIndividualResult() {
        System.out.println();
        System.out.println("Individual results:");
        for (int i = 0; i < this.players.length; i++) {
            if (this.players[i].isOnDistance()) {
                this.players[i].printResult();
            }
        }
    }

    public void printResult() {
        System.out.println();
        for (int i = 0; i < this.players.length; i++) {
            if (!this.players[i].isOnDistance()) {
                System.out.println(this.teamName + " failed!");
                return;
            }
        }
        System.out.println(this.teamName + " successfully finished the marathon!");
    }

    public void printAboutInfo() {
        System.out.println();
        System.out.println(this.teamName + ":");
        for (int i = 0; i < this.players.length; i++) {
            this.players[i].printAboutInfo();
        }
        System.out.println();
    }
}