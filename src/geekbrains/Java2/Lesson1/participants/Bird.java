package geekbrains.Java2.Lesson1.participants;

public class Bird extends Animal {
    public Bird(int maxRunDistance, int maxJumpHeight, int maxSwimDistance, String name) {
        super(maxRunDistance, maxJumpHeight, maxSwimDistance, name);
    }

    @Override
    public void printAboutInfo() {
        System.out.println("Bird "+ name);
    }
}