package geekbrains.Java2.Lesson1.participants;

public interface iChallengable {
    void run(int distance);
    void swim(int distance);
    void jump(int height);
    void printResult();
    void printAboutInfo();
    boolean isOnDistance();
}