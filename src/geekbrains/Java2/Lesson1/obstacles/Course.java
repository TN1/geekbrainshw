package geekbrains.Java2.Lesson1.obstacles;


import geekbrains.Java2.Lesson1.participants.Team;

public class Course {
    private Obstacle[] course = new Obstacle[3];
    private String name;

    public Course(String name, Obstacle[] course) {
        this.course = course;
        this.name = name;
    }

    public void doIt(Team team) {
        System.out.println("Marathon started!!!");
        for (int i = 0; i < team.getPlayers().length; i++) {
            for (int j = 0; j < this.course.length; j++) {
                this.course[j].doIt(team.getPlayers()[i]);
            }
        }
    }
}