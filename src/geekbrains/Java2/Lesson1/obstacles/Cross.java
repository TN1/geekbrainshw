package geekbrains.Java2.Lesson1.obstacles;


import geekbrains.Java2.Lesson1.participants.iChallengable;

public class Cross extends Obstacle {
    int length;

    public Cross(int length) {
        this.length = length;
    }

    @Override
    public void doIt(iChallengable a) {
        a.run(this.length);
    }
}