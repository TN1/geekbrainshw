package geekbrains.Java2.Lesson1.obstacles;


import geekbrains.Java2.Lesson1.participants.iChallengable;

public class Pool extends Obstacle {
    int length;

    public Pool(int length) {
        this.length = length;
    }

    @Override
    public void doIt(iChallengable a) {
        a.swim(this.length);
    }
}