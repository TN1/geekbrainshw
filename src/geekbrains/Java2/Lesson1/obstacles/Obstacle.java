package geekbrains.Java2.Lesson1.obstacles;


import geekbrains.Java2.Lesson1.participants.iChallengable;

public abstract class Obstacle {
    public abstract void doIt(iChallengable challengable);
}
