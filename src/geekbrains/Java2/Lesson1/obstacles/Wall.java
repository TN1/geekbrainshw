package geekbrains.Java2.Lesson1.obstacles;


import geekbrains.Java2.Lesson1.participants.iChallengable;

public class Wall extends Obstacle {
    int height;

    public Wall(int height) {
        this.height = height;
    }

    @Override
    public void doIt(iChallengable a) {
        a.jump(this.height);
    }
}