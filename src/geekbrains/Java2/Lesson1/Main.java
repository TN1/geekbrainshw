package geekbrains.Java2.Lesson1;

import geekbrains.Java2.Lesson1.obstacles.*;
import geekbrains.Java2.Lesson1.participants.*;

public class Main {

    public static void main(String[] args) {
//        iChallengable[] animals = {new Cat(1000, 5, 0, "Barsik"),
//                new Dog(2000, 2, 200, "Sharik")
//        };
//
//        Obstacle[] obstacles = {new Cross(500), new Wall(2), new Pool(100)};
//        for(int i = 0; i < animals.length; i++){
//            for(int j = 0; j < obstacles.length; j++){
//                obstacles[j].doIt(animals[i]);
//            }
//        }
//        System.out.println("Results");
//        animals[0].printResult();
//        animals[1].printResult();

        iChallengable[] players1 = {new Cat(1000, 5, 500, "Barsik"),
                new Dog(2000, 2, 200, "Sharik"),
                new Horse(5000, 4, 400, "Kon"),
                new Human(10000, 5, 1000, "Vasya")};

        iChallengable[] players2 = {new Cat(800, 4, 10, "Murzik"),
                new Dog(1000, 3, 600, "Bim"),
                new Bird(300, 1200, 2000, "Drakon"),
                new Android(10000, 10, 1000, "R2D2")};

        iChallengable[] players3 = {new Android(1000, 5, 500, "C3PO"),
                new Android(1200, 10, 600, "Altron"),
                new Android(1300, 1200, 2000, "Optimus"),
                new Android(10000, 10, 1000, "FZR2000")};

        Obstacle[] obstacles = {new Cross(965), new Wall(4), new Pool(400)};
        Course course = new Course("Marathon", obstacles);

        Team red = new Team("RedTeam", players1);
        Team blue = new Team("BlueTeam", players2);
        Team robots = new Team("Robots_Team", players3);
        red.printAboutInfo();
        course.doIt(red);
        red.printResult();
        red.printIndividualResult();
        System.out.println("****************************************************");
        blue.printAboutInfo();
        course.doIt(blue);
        blue.printResult();
        blue.printIndividualResult();
        System.out.println("****************************************************");
        robots.printAboutInfo();
        course.doIt(robots);
        robots.printResult();
        robots.printIndividualResult();
    }
}