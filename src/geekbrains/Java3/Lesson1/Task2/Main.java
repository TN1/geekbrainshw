package geekbrains.Java3.Lesson1.Task2;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        String[] arr = {"Java", "core", "prof", "GB", "!!!"};
        ArrayList<String> list = toArrayList(arr);
        print(list);
    }

    private static void print(ArrayList<String> list) {
        for (int i = 0; i < list.size(); i++) {
            System.out.print(list.get(i) + " ");
        }
    }

    private static ArrayList<String> toArrayList(String[] arr) {
        ArrayList<String> list = new ArrayList<>();
        for (int i = 0; i < arr.length; i++) {
            list.add(arr[i]);
        }
        return list;
    }
}