package geekbrains.Java3.Lesson1.Task1;

public class Main {
    public static void main(String[] args) {
        String[] arr = {"Java", "core", "prof", "GB", "!!!"};
        print(arr);
        System.out.println();
        swap(1, 4, arr);
        print(arr);
    }

    public static void swap(int first, int second, String[] arr) {
        String temp = arr[first];
        arr[first] = arr[second];
        arr[second] = temp;
    }

    public static void print(String[] arr){
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
    }
}
