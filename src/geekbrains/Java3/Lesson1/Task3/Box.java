package geekbrains.Java3.Lesson1.Task3;

import java.util.ArrayList;

public class Box<T> {
    private ArrayList<T> bag = new ArrayList<>();

    public Box() {

    }

    public Box(T fruit) {
        addFruit(fruit);
    }

    public void addFruit(T fruit) {
        this.bag.add(fruit);
    }

    public ArrayList<T> getBag() {
        return bag;
    }

    public float getWeight() {
        float result = 0.0f;
        if (this.bag.get(0) instanceof Apple) {
            result =  (float) this.bag.size() * ((Apple) this.bag.get(0)).getWeight();
        }

        if (this.bag.get(0) instanceof Orange) {
            result =  (float) this.bag.size() * ((Orange) this.bag.get(0)).getWeight();
        }
        return  result;
    }

    public boolean compare(Box box) {
        if (this.getWeight() == box.getWeight()) {
            return true;
        } else {
            return false;
        }
    }

    public void moveAnotherBox(Box<T> box) {
        for (int i = 0; i < this.bag.size(); i++) {
            box.addFruit(this.bag.get(i));
        }
    }

    public void printInfo() {
        System.out.println();
        System.out.println("Class: " + bag.get(0).getClass());
        System.out.println("Number of fruits: " + bag.size());
        System.out.println("Weight fruits: " + getWeight());
    }
}