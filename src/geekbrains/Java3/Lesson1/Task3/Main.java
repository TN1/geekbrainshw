package geekbrains.Java3.Lesson1.Task3;

public class Main {
    public static void main(String[] args) {
        Apple apple1 = new Apple();
        Apple apple2 = new Apple();
        Apple apple3 = new Apple();
        Apple apple4 = new Apple();

        Orange orange1 = new Orange();
        Orange orange2 = new Orange();
        Orange orange3 = new Orange();

        Box<Apple> appleBox = new Box<>(apple1);
        Box<Orange> orangeBox = new Box<>(orange1);

        appleBox.addFruit(apple2);
        appleBox.addFruit(apple3);
        appleBox.addFruit(apple4);

        orangeBox.addFruit(orange2);
        orangeBox.addFruit(orange3);

        appleBox.printInfo();
        orangeBox.printInfo();

        System.out.println(appleBox.compare(orangeBox));

        Apple apple5 = new Apple();
        Apple apple6 = new Apple();
        Box<Apple> newAppleBox = new Box<>();
        newAppleBox.addFruit(apple5);
        newAppleBox.addFruit(apple6);

        appleBox.moveAnotherBox(newAppleBox);

        appleBox.printInfo();
        newAppleBox.printInfo();
    }
}
