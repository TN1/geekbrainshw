package geekbrains.Java3.Lesson2;

import java.sql.*;

public class Database {
    private static Connection connection;   // Объект хранит коннект к БД.
    private static Statement statement;     // Объект используется для хранения и выполнения SQL-запросов.
    private static PreparedStatement ps;    // Объект хранит скомпилированную версию SQL-выражения для быстрого

    public static void createDB() {
        try {
            connectDB();
            dropTable();
            createTable();
            clearTable();
            addData();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            disconnect();
        }
    }

    public static void getMoreProducts(int fromCost, int toCost) throws SQLException {
            ResultSet resultSet = statement.executeQuery("SELECT id, prodid, title, cost FROM products WHERE cost BETWEEN " + fromCost +" AND " + toCost +";");
            while(resultSet.next()) {
                System.out.println(resultSet.getInt(1) + " " + resultSet.getInt(2) +
                        " " + resultSet.getString("title") + " " +resultSet.getInt(4));
            }
    }

    private static int getCountProduct(String product) throws SQLException {
        ResultSet countResult = statement.executeQuery("SELECT COUNT(*) FROM products WHERE title = " + "'" + product + "';");
        return countResult.getInt(1);
    }

    public static void getProductsCost (String product) throws SQLException {
        if (getCountProduct(product) == 0) {
            System.out.println("Product " + "\"" + product +"\"" + " not found!");
        } else {
            ResultSet resultSet = statement.executeQuery("SELECT id, prodid, title, cost FROM products WHERE title = " + "'" + product + "';");
            while(resultSet.next()) {
                System.out.println(resultSet.getInt(1) + " " + resultSet.getInt(2) +
                        " " + resultSet.getString("title") + " " +resultSet.getInt(4));
            }
        }
    }

    public static void changeProductCost(String product, int cost) throws SQLException {
        if (getCountProduct(product) != 0) {
            statement.executeUpdate("UPDATE products SET cost = " + cost + " WHERE title = " + "'" + product + "'");
        } else {
            System.out.println("Product " + product + " not found!");
        }
    }

    private static void dropTable() throws SQLException{
        statement.execute("DROP TABLE products");
    }

    private static void addData() throws SQLException {
        connection.setAutoCommit(false);
        ps = connection.prepareStatement("INSERT INTO products(prodid, title, cost) VALUES (?,?,?);");
        for(int i = 1; i <= 10_000; i++){
            ps.setInt(1, i);
            ps.setString(2, "product" + i);
            ps.setInt(3, i*10);
            ps.addBatch();
        }
        ps.executeBatch();
        connection.commit();
    }

    private static void clearTable() throws SQLException {
        statement.execute("DELETE FROM products");
    }

    public static void disconnect() {
        try{
            statement.close();
            connection.close();
        }catch(SQLException e){
            e.printStackTrace();
        }
    }

    private static void createTable() throws SQLException {
        statement.execute("CREATE TABLE IF NOT EXISTS products (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, prodid INTEGER NOT NULL, title text NOT NULL, cost INTEGER NOT NULL);");
    }

    public static void connectDB() throws ClassNotFoundException, SQLException {
        Class.forName("org.sqlite.JDBC"); // Класс для работы с БД sqlLite
        connection = DriverManager.getConnection("jdbc:sqlite:java3lesson2.db"); // Установка переменной для коннекта к БД
        statement = connection.createStatement();
    }
}