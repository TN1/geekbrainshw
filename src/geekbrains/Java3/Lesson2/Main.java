package geekbrains.Java3.Lesson2;

import java.sql.SQLException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        Scanner scanner = new Scanner(System.in);
        Database.createDB();
        while (true) {
            Database.connectDB();
            String in = scanner.nextLine();
            String[] elements = in.split(" ");

            if (in.equalsIgnoreCase("/end")) {
                Database.disconnect();
                System.out.println("End application");
                break;
            } else if (elements[0].equalsIgnoreCase("/cost")) {
                Database.getProductsCost(elements[1]);
            } else if (elements[0].equalsIgnoreCase("/changecost")) {
                Database.changeProductCost(elements[1], Integer.parseInt(elements[2]));
            } else if (elements[0].equalsIgnoreCase("/productscost")) {
                Database.getMoreProducts(Integer.parseInt(elements[1]), Integer.parseInt(elements[2]));
            } else {
                System.out.println("Command not correct!");
            }
        }
    }
}