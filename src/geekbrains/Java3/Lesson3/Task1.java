package geekbrains.Java3.Lesson3;

import java.io.*;

public class Task1 {
    public static void main(String[] args) {
        InputStream inFile = null;
        ByteArrayOutputStream outByteArr = null;
        try {
            inFile = new FileInputStream("files/task1.txt"); //создаем вход поток и передаем ему на вход файл
            byte[] arr = new byte[50];
            outByteArr = new ByteArrayOutputStream(); //выход поток
            int data;
            while((data = inFile.read(arr)) != -1) {
                outByteArr.write(arr, 0, data);
            }
            arr = outByteArr.toByteArray();
            for (int i = 0; i < arr.length; i++) {
                System.out.print((char) arr[i]);
            }

        } catch (IOException e) {
            e.getStackTrace();
        } finally {
            try {
                inFile.close();
                outByteArr.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}