package geekbrains.Java3.Lesson3;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;

public class Task2 {
    public static void main(String[] args) {
        ArrayList<InputStream> inFilesArray = new ArrayList<>();
        OutputStream outFile = null;
        SequenceInputStream seqIn = null;
        InputStream inFile = null;
        try {
            outFile = new FileOutputStream("files/task2_out");
            inFilesArray.add(new FileInputStream("files/task2_0"));
            inFilesArray.add(new FileInputStream("files/task2_1"));
            inFilesArray.add(new FileInputStream("files/task2_2"));
            inFilesArray.add(new FileInputStream("files/task2_3"));
            inFilesArray.add(new FileInputStream("files/task2_4"));
            Enumeration<InputStream> en = Collections.enumeration(inFilesArray);
            seqIn = new SequenceInputStream(en);
            int data;
            while ((data = seqIn.read()) != -1){
                outFile.write(data);
            }
            inFile = new FileInputStream("files/task2_out");
            while ((data = inFile.read()) != -1) {
                System.out.print((char) data);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.getStackTrace();
        } finally {
            try {
                seqIn.close();
                outFile.close();
                inFile.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}