package geekbrains.Java3.Lesson3;

import java.io.*;
import java.util.ArrayList;

public class Task3 {
    public static void main(String[] args) throws IOException {
        String book = "files/tlotr.txt";
        readPage(book, 5);
    }

    public static void readPage(String book, int pageNumber) {
        InputStream in = null;
        try {
            ArrayList<byte[]> inPages = new ArrayList<>();
            int sizePage = 1800;
            byte[] tempArray = new byte[sizePage];
            in = new FileInputStream(book);
            int x;
            while((x = in.read(tempArray, 0, sizePage)) != -1) {
                byte[] bArr = new byte[sizePage];
                for (int i = 0; i < tempArray.length; i++) {
                    bArr[i] = tempArray[i];
                }
                inPages.add(bArr);
            }
            byte[] page = inPages.get(pageNumber);
            for (int i = 0; i < page.length; i++) {
                System.out.print((char) page[i]);
            }
        } catch (IOException e) {
            e.getStackTrace();
        } finally {
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}