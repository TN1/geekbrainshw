package geekbrains.Java3.Lesson4;

public class Task1 {
    public static Object monitor = new Object();
    public static char ch = 'A';
    public static void main(String[] args) {
        new Thread(() -> {
            try {
                for (int i = 0; i < 5; i++) {
                    synchronized (monitor) {
                        while (ch != 'A'){
                            monitor.wait();
                        }
                        System.out.print(ch + " ");
                        ch = 'B';
                        monitor.notifyAll();
                    }
                }
            } catch (InterruptedException e) {
                e.getStackTrace();
            }
        }).start();

        new Thread(() -> {
            try {
                for (int i = 0; i < 5; i++) {
                    synchronized (monitor) {
                        while (ch != 'B'){
                            monitor.wait();
                        }
                        System.out.print(ch + " ");
                        ch = 'C';
                        monitor.notifyAll();
                    }
                }
            } catch (InterruptedException e) {
                e.getStackTrace();
            }
        }).start();

        new Thread(() -> {
            try {
                for (int i = 0; i < 5; i++) {
                    synchronized (monitor) {
                        while (ch != 'C'){
                            monitor.wait();
                        }
                        System.out.print(ch + " ");
                        ch = 'A';
                        monitor.notifyAll();
                    }
                }
            } catch (InterruptedException e) {
                e.getStackTrace();
            }
        }).start();
    }
}