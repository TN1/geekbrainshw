package geekbrains.Java3.Lesson4;

import java.io.FileWriter;
import java.io.IOException;

public class WriteFile implements Runnable{
    FileWriter fileWriter;
    public WriteFile(FileWriter fileWriter){
        this.fileWriter = fileWriter;
    }
    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            try {
                fileWriter.write(Thread.currentThread().getName() + " write " + i + "-string in file\n");
                System.out.println(Thread.currentThread().getName());
                Thread.sleep(20);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
}
