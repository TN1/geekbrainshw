package geekbrains.Java3.Lesson4;

public class MFU {
    Object printerMonitor = new Object();
    Object scannerMonitor = new Object();
    public void printer(String book) {
        new Thread(() -> {
            synchronized (printerMonitor) {
                System.out.println("Printing " + book);
                for (int i = 0; i < 10; i++) {
                    System.out.println("Printed" + i + " page");
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
            System.out.println("Printing " + book + " complete");
        }).start();
    }

    public void scanner(String book) {
        new Thread(() -> {
            synchronized (scannerMonitor) {
                System.out.println("Scanning " + book);
                for (int i = 0; i < 10; i++) {
                    System.out.println("Scanned " + i + " page");
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
            System.out.println("Scanning " + book + " complete");
        }).start();
    }
}