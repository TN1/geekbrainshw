package geekbrains.Java3.Lesson4;

public class Task3 {
    public static void main(String[] args) {
        String book1 = "Book #1";
        String book2 = "Book #2";
        String book3 = "Book #3";
        String book4 = "Book #4";

        MFU mfu = new MFU();

        mfu.printer(book1);
        mfu.printer(book2);

        mfu.scanner(book3);
        mfu.scanner(book2);

        mfu.printer(book1);
        mfu.printer(book4);
        mfu.printer(book4);

        mfu.scanner(book3);
        mfu.scanner(book4);
        mfu.scanner(book4);
        mfu.scanner(book3);

        mfu.printer(book3);
        mfu.printer(book1);

        mfu.scanner(book1);
    }
}