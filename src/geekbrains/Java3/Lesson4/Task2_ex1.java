package geekbrains.Java3.Lesson4;

import java.io.FileWriter;
import java.io.IOException;

//запись поочередно потоками в файл, аналогично задаче 1, без задержки в 20 мс (для практики)
public class Task2_ex1 {
    public static FileWriter fileWriter;
    public static String checkThread = "t1";
    public static void main(String[] args) {
        try {
            fileWriter = new FileWriter("files/lesson4_task2_ex1");
            writeStringThread();
        } catch (IOException e) {
            e.getStackTrace();
        }
    }

    private static void writeStringThread() {
        new Thread(() -> {
            try {
                String str = "(T1) Write string t1\n";
                for (int i = 0; i < 10; i++) {
                    synchronized (fileWriter) {
                        while (!checkThread.equals("t1")){
                            fileWriter.wait();
                        }
                        fileWriter.write(str);
                        checkThread = "t2";
                        fileWriter.notifyAll();
                    }
                }
            } catch (InterruptedException e) {
                e.getStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();

        new Thread(() -> {
            try {
                String str = "(T2) Write string t2\n";
                for (int i = 0; i < 10; i++) {
                    synchronized (fileWriter) {
                        while (!checkThread.equals("t2")){
                            fileWriter.wait();
                        }
                        fileWriter.write(str);
                        checkThread = "t3";
                        fileWriter.notifyAll();
                    }
                }
            } catch (InterruptedException e) {
                e.getStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();

        new Thread(() -> {
            try {
                String str = "(T3) Write string t3\n";
                for (int i = 0; i < 10; i++) {
                    synchronized (fileWriter) {
                        while (!checkThread.equals("t3")){
                            fileWriter.wait();
                        }
                        fileWriter.write(str);
                        checkThread = "t1";
                        fileWriter.notifyAll();
                    }
                }
            } catch (InterruptedException e) {
                e.getStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    fileWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
