package geekbrains.Java3.Lesson4;

import java.io.FileWriter;
import java.io.IOException;

public class Task2_ex2 {
    public static FileWriter fileWriter;
    public static void main(String[] args) {
        try {
            fileWriter = new FileWriter("files/lesson4_task2_ex2");
            Thread thread1 = new Thread(new WriteFile(fileWriter));
            Thread thread2 = new Thread(new WriteFile(fileWriter));
            Thread thread3 = new Thread(new WriteFile(fileWriter));
            thread1.start();
            thread2.start();
            thread3.start();
            thread1.join();
            thread2.join();
            thread3.join();
        } catch (IOException e) {
            e.getStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            try {
                fileWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}