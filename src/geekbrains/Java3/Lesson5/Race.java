package geekbrains.Java3.Lesson5;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Eliseev Vasiliy on 12.03.2018.
 */
public class Race {
    private ArrayList<Stage> stages;
    public ArrayList<Stage> getStages() { return stages; }
    public Race(Stage... stages) {
        this.stages = new ArrayList<>(Arrays.asList(stages));
    }
}
