package geekbrains.Java3.Lesson5;

/**
 * Created by Eliseev Vasiliy on 12.03.2018.
 */
public abstract class Stage {
    protected int length;
    protected String description;
    public String getDescription() {
        return description;
    }
    public abstract void go(Car c);
}
