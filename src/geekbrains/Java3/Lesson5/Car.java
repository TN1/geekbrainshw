package geekbrains.Java3.Lesson5;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Eliseev Vasiliy on 12.03.2018.
 */
public class Car implements Runnable {
    private static int CARS_COUNT;
    static {
        CARS_COUNT = 0;
    }
    public static CyclicBarrier carThreads;
    private static AtomicInteger atomInt = new AtomicInteger(0);
    private Race race;
    private int speed;
    private String name;
    public String getName() {
        return name;
    }
    public int getSpeed() {
        return speed;
    }
    public Car(Race race, int speed, CyclicBarrier carThreads) {
        this.race = race;
        this.speed = speed;
        Car.carThreads = carThreads;
        CARS_COUNT++;
        this.name = "Участник #" + CARS_COUNT;

    }
    @Override
    public void run() {
        try {
            System.out.println(this.name + " готовится");
            Thread.sleep(500 + (int)(Math.random() * 800));
            System.out.println(this.name + " готов");
            carThreads.await();
        } catch (Exception e) {
            e.printStackTrace();
        }
        for (int i = 0; i < race.getStages().size(); i++) {
            race.getStages().get(i).go(this);
        }
        if(atomInt.incrementAndGet() == 1){
            System.out.println(this.name + " WIN !!!");
        }
        try {
            carThreads.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (BrokenBarrierException e) {
            e.printStackTrace();
        }
    }
}
